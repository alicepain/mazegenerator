import numpy as np
import random
import pygame
import sys
import time

#GLOBAL VARIABLES
SLEEP = 0
DIMENSIONS = (60,60)

class Game():

    def __init__(self, dimensions, start, unit):
        pygame.init()
        self.dimensions = dimensions
        self.current = start
        self.unit = unit
        self.stack = [(0, 0)]
        self.visited = [(0, 0)]
        self.links = []
        self.nSquares = self.dimensions[0] * self.dimensions[1]
        self.directions = [np.array([0,1]), np.array([0,-1]), np.array([1,0]), np.array([-1,0])]
        self.screen = pygame.display.set_mode((self.dimensions[0] * self.unit, self.dimensions[1] * self.unit))
        self.clock = pygame.time.Clock()

    def getRect(self, square):
        return np.array([square[0], square[1], 1, 1]) * self.unit

    def drawSquare(self, square, color):
        pygame.draw.rect(self.screen, color, self.getRect(square))

    def drawVisited(self):
        i = 255
        for square in self.visited:
            self.drawSquare(square, (255, 255, 255)) #255, i, 0
            i -= 255 // self.nSquares

    def drawCurrent(self):
        self.drawSquare(self.current, (0, 0, 0)) #0, 0, 0

    def getLineRect(self, start, direction):
        if direction[0] >= 0 and direction[1] >= 0:
            return np.array([start[0] * self.unit, start[1] * self.unit, 2 + direction[0] * self.unit, 2 + direction[1] * self.unit])

        else:
            end = start + direction
            direction = - direction
            return np.array([end[0] * self.unit, end[1] * self.unit, 2 + direction[0] * self.unit, 2 + direction[1] * self.unit])

    def drawLine(self, start, direction):
        pygame.draw.rect(self.screen, (0, 0, 0), self.getLineRect(start, direction))

    def drawLines(self, links):
        for link in links:
            self.drawLine(link[0], link[1])

    def draw(self):
        self.screen.fill((255, 255, 255))
        self.drawVisited()
        self.drawCurrent()
        self.drawLines(self.links)
        pygame.display.update()

    def step(self):
        adjacent = [self.current + direction for direction in self.directions]
        adjacent = [adj for adj in adjacent if 0 <= adj[0] < self.dimensions[0] and 0 <= adj[1] < self.dimensions[1] and tuple(adj) not in self.visited]
        if adjacent:
            new = random.choice(adjacent)
            newTuple = tuple(new)
            self.stack.append(newTuple)
            self.visited.append(newTuple)
            self.links.append((self.current, new - self.current))
            return new
        else:
            self.stack.pop()
            return np.array(self.stack[-1])

    def run(self):
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    pygame.quit()
                    sys.exit()

            if len(self.visited) < self.nSquares:
                self.current = self.step()

            self.draw()
            self.clock.tick(120)
            time.sleep(SLEEP)

if __name__ == "__main__":
    screenMax = (1200, 800)
    ratio = (screenMax[0] // DIMENSIONS[0], screenMax[1] // DIMENSIONS[1])
    unit = min(ratio)
    start = np.array([0,0])
    game = Game(DIMENSIONS, start, unit)
    game.run()
    
